# -*- coding: utf-8 -*-
import codecs
import os
import ast
import pip
from collections import namedtuple
from optparse import OptionParser
from subprocess import call

def main():
    # init option parser
    parser = OptionParser(
        prog="dependency_manager.py",
        version="%prog 1",
        usage="%prog [options] -f 'source file' -i 'information mode' \n")

    parser.add_option("-f", "--input-file", dest="input",
                      help="Source file")
    parser.add_option("-i", "--information-mode", dest="inform_mode", action="store_true", default=False,
                      help="Show requirements information without installation")

    # generate and parse args
    (options, args) = parser.parse_args()

    # check input-file arg
    if options.input is None:
        parser.error("-f arg is None")
    else:
        if os.path.exists(options.input):
            file_path = options.input
        else:
            parser.error("File not exist")

    # check inform arg
    __inform_mode = options.inform_mode

    # main flow
    package_tuples = parse_imports(file_path)

    if(__inform_mode):
        show_information(package_tuples)
    else:
        download_modules_through_pip(package_tuples)


def show_information(packages):
    for import_item in packages:
        print(import_item)


def parse_imports(file_path):
    # init
    Import = namedtuple("Import", ["module", "name"])
    file = codecs.open(file_path, "r", "utf-8")

    # normalize file
    file_data = ""

    for line in file:
        if line.startswith("import") or line.startswith("from"):
            file_data += line

    # create ast tree
    root = ast.parse(file_data)
    for node in ast.iter_child_nodes(root):
        if isinstance(node, ast.Import):
            module = []
        elif isinstance(node, ast.ImportFrom):
            module = node.module.split('.')
        else:
            continue

        for n in node.names:
            if len(module) == 0:
                yield Import(n.name.split('.'), [])
            else:
                yield Import(module, n.name.split('.'))

    file.close()


def download_modules_through_pip(package_tuples):
    # tuple to packages list
    packages = []
    for tuple in package_tuples:
        if packages.count((tuple[0])[0]):
            packages.append((tuple[0])[0])

    # load through pip
    for package in packages:
        # if call("pip install " + package, shell=False) == 1:
        show_message("Performing download " + package)

        # check error
        if pip.main(['install', package]) == 1:
            show_message(package + " dependency cannot be satisfied")
        else:
            show_message(package + " dependency installing success")

def show_message(message):
    print("\n//\n//\n" + message + "\n//\n//\n")


if __name__ == "__main__":
    main()