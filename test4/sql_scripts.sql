
--// create maindb table
CREATE TABLE main (
    id serial PRIMARY KEY,
    text VARCHAR (255) NOT NULL
    );

--// create datedb table
CREATE TABLE date (
    pdate DATE PRIMARY KEY,
    counter INTEGER NOT NULL
    );

--// add item to maindb
INSERT INTO main VALUES (1, 'lalatext');

--// get all from main
SELECT * FROM main;

--delete all from main
DELETE FROM main;

--// get all from date
SELECT * FROM date;

--// get all from date
SELECT * FROM date;

--// add trigger
CREATE TRIGGER new_record AFTER INSERT ON main FOR EACH ROW EXECUTE PROCEDURE add_to_log();

--// init trigger
CREATE OR REPLACE FUNCTION add_to_log()
    RETURNS trigger AS
$$
plpy.execute("SELECT dblink_connect('dbname=datedb')")

cnt_rows = plpy.execute("SELECT * FROM dblink('SELECT * FROM date') AS t(pdate date, cnt int) WHERE pdate = CURRENT_DATE")

if len(cnt_rows) == 0:
    rv = plpy.execute("SELECT dblink_exec('dbname=datedb', 'INSERT INTO date VALUES (CURRENT_DATE, 1)')")
else:
    rv = plpy.execute("SELECT dblink_exec('dbname=datedb', 'UPDATE date SET counter = {counter} WHERE pdate = CURRENT_DATE')".format(counter = cnt_rows[0]["cnt"] + 1))
$$
LANGUAGE plpythonu;