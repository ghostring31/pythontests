# -*- coding: utf-8 -*-
from optparse import OptionParser

import sys

from gmail_manager import ApiManager
from langdetect import detect
import quopri
import codecs

en_subject = u'Greetings from the Galactic Republic'
de_subject = u'Für die Republik !! (c) Stormtrooper'

answers_dictionary = {
    'en': 'en_answer.txt',
    'de': 'de_answer.txt'
}

subjects_dictionary = {
    'en': en_subject,
    'de': de_subject
}

# init sdk
gmail_manager = ApiManager()


def analyze_message(message):
    # # debug
    # print(message['snippet'])
    # print(detect(message['snippet']))

    # select message/subject depends on language
    try:
        answer_path = answers_dictionary[detect(message['snippet'])]
        subject = subjects_dictionary[detect(message['snippet'])]
    except KeyError as e:
        # default de
        answer_path = answers_dictionary['de']
        subject = subjects_dictionary['de']

    target_message = codecs.open(answer_path, "r", "utf-8").read()

    # retrieve message params
    to_mail = ''
    from_mail = ''
    for header in message['payload']['headers']:
        if header['name'] == 'From':
            to_mail = header['value']
            break

    for header in message['payload']['headers']:
        if header['name'] == 'To':
            from_mail = header['value']
            break

    # sending message
    if from_mail and to_mail != '':
        reply_message = gmail_manager.create_message(from_mail, to_mail, subject, target_message)
        gmail_manager.send_message(reply_message)

        # mark message as 'readed'
        gmail_manager.mark_message_as_read(message['id'])
    else:
        print('Error reading parameters')

def main():
    # init option parser
    parser = OptionParser(
        prog="vocation_responder.py",
        version="%prog 1",
        usage="%prog [options] -e 'email folder'\n")

    parser.add_option("-e", "--email-folder", dest="email_folder", default='INBOX', help="EMAIL FOLDER")

    # generate and parse args
    (options, args) = parser.parse_args()

    email_folder = options.email_folder

    # main flow

    # check email folder exist
    labels = gmail_manager.get_labels()

    if not labels:
        print('No labels found.')
    else:
      print('Labels:')
      for label in labels:
        print(label['name'])

    labels_data = ['UNREAD']

    # check inputted label
    check_folder = False
    for label in labels:
        if label['name'] == email_folder:
            check_folder = True

    if not check_folder:
        sys.exit("Email folder doesn't exist")

    labels_data.append(email_folder)

    # load messages
    messages = gmail_manager.get_messages(labels_data)

    if not messages:
        print('No messages found.')
    else:
        for message in messages:
            analyze_message(gmail_manager.get_message_detail(message['id']))

        # debug
        # analyze_message(gmail_manager.get_message_detail(messages[1]['id']))

if __name__ == '__main__':
    main()