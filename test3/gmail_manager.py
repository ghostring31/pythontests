# -*- coding: utf-8 -*-
import base64
import os
from email.mime.text import MIMEText

import httplib2
import sys
from googleapiclient import discovery
from googleapiclient import errors
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage


class ApiManager:
    # __SCOPES = 'https://www.googleapis.com/auth/gmail.compose'
    __SCOPES = 'https://mail.google.com/'
    __CLIENT_SECRET_FILE = 'client_secret.json'
    __APPLICATION_NAME = 'PyTest'

    __SERVICE = None

    def get_credentials(self):
        """Gets valid user credentials from storage.

        If nothing has been stored, or if the stored credentials are invalid,
        the OAuth2 flow is completed to obtain the new credentials.

        Returns:
            Credentials, the obtained credential.
        """
        home_dir = os.path.expanduser('~')
        credential_dir = os.path.join(home_dir, '.credentials')
        if not os.path.exists(credential_dir):
            os.makedirs(credential_dir)
        credential_path = os.path.join(credential_dir, 'gmail-python-quickstart.json')

        store = Storage(credential_path)
        credentials = store.get()
        if not credentials or credentials.invalid:
            flow = client.flow_from_clientsecrets(self.__CLIENT_SECRET_FILE, self.__SCOPES)
            flow.user_agent = self.__APPLICATION_NAME
            credentials = tools.run_flow(flow, store)
            print('Storing credentials to ' + credential_path)
        return credentials

    def get_labels(self):
        results = self.__SERVICE.labels().list(userId='me').execute()
        return results.get('labels', [])

    def get_messages(self, label_ids):
        results = self.__SERVICE.messages().list(userId='me', maxResults=sys.maxint, includeSpamTrash=False, labelIds=label_ids).execute()
        return results.get('messages', [])

    def get_message_detail(self, message_id):
        return self.__SERVICE.messages().get(userId='me', id=message_id, format='full').execute()

    def create_message(self, sender, to, subject, message_text):
        """Create a message for an email.

        Args:
          sender: Email address of the sender.
          to: Email address of the receiver.
          subject: The subject of the email message.
          message_text: The text of the email message.

        Returns:
          An object containing a base64url encoded email object.
        """
        message = MIMEText(message_text)
        message['to'] = to
        message['from'] = sender
        message['subject'] = subject
        return {'raw': base64.urlsafe_b64encode(message.as_string())}


    def send_message(self, message):
        """Send an email message.

        Args:
          service: Authorized Gmail API service instance.
          user_id: User's email address. The special value "me"
          can be used to indicate the authenticated user.
          message: Message to be sent.

        Returns:
          Sent Message.
        """
        try:
            message = self.__SERVICE.messages().send(userId='me', body=message).execute()
            print('Message Id: %s' % message['id'])
            return message
        except errors.HttpError, error:
            print 'An error occurred: %s' % error


    def mark_message_as_read(self, message_id):
        self.__SERVICE.messages().modify(userId='me', id=message_id,
                                                body={'removeLabelIds': ['UNREAD']}).execute()

    def __init__(self):
        # init gmail api
        credentials = self.get_credentials()
        http = credentials.authorize(httplib2.Http())
        self.__SERVICE = discovery.build('gmail', 'v1', http=http).users()